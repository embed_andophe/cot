# ifndef COT_PREPROCESSOR_LOGICAL_COMPL_H
# define COT_PREPROCESSOR_LOGICAL_COMPL_H
#
#
# /* COT_PP_COMPL */
#

#    define COT_PP_COMPL(x) COT_PP_COMPL_I(x)


#    define COT_PP_COMPL_I(x) COT_PP_COMPL_ ## x

#
# define COT_PP_COMPL_0 1
# define COT_PP_COMPL_1 0
#
# endif